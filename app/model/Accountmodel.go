package model

import (
	"begorm/app/utils"
	"fmt"

	"github.com/pkg/errors"
	"gorm.io/gorm"
)

type Account struct {
	DB            *gorm.DB
	ID            int    `gorm:"primary_key" json:"-"`
	IdAccount     string `json:"id_account,omitempty"`
	Name          string `json:"name"`
	Password      string `json:"password,omitempty"`
	AccountNumber int    `json:"account_number,omitempty"`
	Saldo         int    `json:"saldo"`
}

func InsertNewAccount(account Account) (bool, error) {
	account.AccountNumber = utils.RangeIn(111111, 999999)
	account.Saldo = 0
	account.IdAccount = fmt.Sprintf("id-%d", utils.RangeIn(111, 999))

	if err := DB.Create(&account).Error; err != nil {
		return false, errors.Errorf("invalid prepare statement :%+v\n", err)
	}

	return true, nil
}

func GetAccountDetail(idAccount int) (bool, error, []Transaction, Account) {
	var transaction []Transaction
	var account Account

	if err := DB.Where("sender = ? OR recipient = ?", idAccount, idAccount).Find(&transaction).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return false, errors.Errorf("Account not found"), []Transaction{}, Account{}
		} else {
			return false, errors.Errorf("invalid prepare statement :%+v\n"), []Transaction{}, Account{}
		}
	}

	if err := DB.Where(&Account{AccountNumber: idAccount}).Find(&account).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return false, errors.Errorf("Account not found"), []Transaction{}, Account{}
		} else {
			return false, errors.Errorf("invalid prepare statement :%+v\n"), []Transaction{}, Account{}
		}
	}

	return true, nil, transaction, Account{
		IdAccount:     account.IdAccount,
		Name:          account.Name,
		AccountNumber: account.AccountNumber,
		Saldo:         account.Saldo,
	}
}
